$ ->
  $src = $('#src')
  $output = $('#output')
  $review = $('#review')

  brush = new SyntaxHighlighter.brushes.Php()
  brush.init toolbar: false

  $template = $('''
    <div class="syntaxhighlighter">
      <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td class="gutter">
            </td>
            <td class="code">
              <div class="container">
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="well">
      <textarea class="comment span12"></textarea>
      <div class="post">
        <a href="#" class="btn btn-mini btn-primary">コメント</a>
      </div>
    </div>
                        ''')

  setup = ->
    src = $src.val()
    html = brush.getHtml(src)
    $html = $(html)
    $gutter = $html.find('.gutter')
    $code = $html.find('.code')
    $output.empty().append($html)
    $html.find('.line').off().on('click', (event)->
      $line = $(event.target)
      tag_name = event.target.tagName.toLowerCase()
      if tag_name is 'code'
        $line = $line.parents('.line')

      classes = $line.attr('class')
      matches = classes.match(/number([0-9]+)/)
      return false if matches is null

      number = parseInt(matches[1], 10)
      return false if number < 1

      find_class = '.number' + number.toString()
      $line_gutter = $gutter.find(find_class)
      $line_code = $code.find(find_class)

      $row = $template.clone()

      $row.find('.gutter').append($line_gutter.clone())
      $row.find('.code .container').append($line_code.clone())

      $review.prepend($row);

      $row.find('.post a').off().on('click', ->
        $btn = $(@)
        $container = $btn.parents('.well')
        $comment = $container.find('.comment')
        $container.html('<p>' + $comment.val().replace(/\r\n|\r|\n|\n\r/g, '<br>') + '</p>')
      )
      false
    )

  $src.on('keyup', ->
    setup()
  )
  setup() #init

  $window = $(window)
  resize_src = ->
    $src.height($window.height() - 100)
  $window.on('resize', ->
    resize_src()
  )
  resize_src()
